import React, { FC } from 'react';
import Type from './Type';
import styled from 'styled-components';

const StyledWeight = styled.div`
  text-align: center;

  .weight {
    &__no {
      font-weight: 600;
      text-transform: uppercase;
    }

    &__label {
      display: block;
      font-size: 0.75rem;
    }
  }
`;

type WeightProps = {
  weight: number;
};

const Weight: FC<WeightProps> = ({ weight }) => (
  <StyledWeight className="weight">
    <span className="weight__no">{weight} kg</span>
    <span className="weight__label">Weight</span>
  </StyledWeight>
);

export default Weight;
