import React, { FC } from 'react';
import { useRouter } from 'next/router';
import Type from './Type';
import styled from 'styled-components';

const StyledHeader = styled.div`
  background-color: #fff;
  border-radius: 0 0 25px 25px;
  padding: 1.111rem 1.111rem 1.667rem;

  .header {
    &__content {
      align-items: center;
      display: flex;
      justify-content: space-between;
      margin-bottom: 1.111rem;
    }

    &__id {
      color: #000;
      font-weight: 600;
    }

    &__btn {
      appearance: none;
      background: transparent;
      border: none;
      cursor: pointer;
      font-weight: 600;
      font-size: 1rem;
      padding: 0;
    }

    &__image-container {
      display: flex;
      justify-content: center;
      margin: 0 auto;
    }
  }
`;

type HeaderProps = {
  id: number;
  name: string;
  image: string;
};

const Header: FC<HeaderProps> = ({ id, name, image }) => {
  const router = useRouter();

  return (
    <StyledHeader className="header__header">
      <div className="header__content">
        <button
          className="header__btn"
          onClick={() => router.back()}
          type="button"
        >
          &#8249; Pokédex
        </button>

        <p className="header__id">#{id}</p>
      </div>

      <div className="header__image-container">
        <img
          className="header__image"
          alt={name}
          height={150}
          src={image}
          width={150}
        />
      </div>
    </StyledHeader>
  );
};

export default Header;
