import React, { FC } from 'react';
import Type from './Type';
import styled from 'styled-components';

const StyledHeight = styled.div`
  text-align: center;

  .height {
    &__no {
      font-weight: 600;
      text-transform: uppercase;
    }

    &__label {
      display: block;
      font-size: 0.75rem;
    }
  }
`;

type HeightProps = {
  height: number;
};

const Height: FC<HeightProps> = ({ height }) => (
  <StyledHeight className="height">
    <span className="height__no">{height} m</span>
    <span className="height__label">Height</span>
  </StyledHeight>
);

export default Height;
