import React, { FC } from 'react';
import Type from './Type';
import styled from 'styled-components';
import Stat from '../components/Stat';

const StyledStats = styled.div`
  .stats {
    &__title {
      font-weight: 600;
      text-align: center;
    }

    &__list {
      margin-top: 0.278rem;
    }
  }
`;

type StatsProps = {
  stats: Array<{
    base_stat: number;
    stat: {
      name: string;
    };
  }>;
};

const Stats: FC<StatsProps> = ({ stats }) => (
  <StyledStats className="stats">
    <p className="stats__title">Base stats</p>

    <ul className="stats__list">
      {stats.map((stat, index) => (
        <Stat key={index} {...stat} />
      ))}
    </ul>
  </StyledStats>
);

export default Stats;
