import React, { FC } from 'react';
import styled from 'styled-components';
import PokemonItem from './PokemonItem';

const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  justify-content: center;
`;

type PokemonListProps = {
  allPokemon: Array<{
    name: string;
    url: string;
  }>;
};

const PokemonList: FC<PokemonListProps> = ({ allPokemon }) => (
  <List>
    {allPokemon.map((pokemon, index) => (
      <PokemonItem key={index} {...pokemon} />
    ))}
  </List>
);

export default PokemonList;
