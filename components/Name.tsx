import React, { FC } from 'react';
import Type from './Type';
import styled from 'styled-components';

const PokemonName = styled.h1`
  font-weight: 600;
  text-align: center;
  text-transform: capitalize;
`;

type NameProps = {
  name: string;
};

const Name: FC<NameProps> = ({ name }) => <PokemonName>{name}</PokemonName>;

export default Name;
