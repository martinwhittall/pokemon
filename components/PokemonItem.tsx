import React, { FC, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import styled from 'styled-components';

import { PokemonProps } from './../types/Pokemon';
import { PokemonItemProps } from './../types/PokemonItem';

const PokemonTile = styled.li`
  background-color: #fff;
  border-radius: 25px;
  cursor: pointer;
  display: block;
  padding: 0.556rem;
  position: relative;
  margin: 0.278rem;
  max-width: 177px;
  width: calc(100% - 0.556rem);

  &:hover {
    background-color: rgba(255, 255, 255, 0.9);
  }
`;

const Image = styled.img`
  display: flex;
  height: 100px;
  margin: 0 auto;
  width: 100px;

  &::after {
    content: '';
    display: block;
    padding-top: 100%;
  }
`;

const Name = styled.a`
  color: #000;
  display: block;
  font-weight: 500;
  margin: 0.556rem 0 0 0;
  text-transform: capitalize;
  text-align: center;

  &::after {
    bottom: 0;
    content: '';
    display: block;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
  }
`;

const PokemonItem: FC<PokemonItemProps> = ({ name, url }) => {
  const router = useRouter();

  const [pokemon, setPokemon] = useState<PokemonProps | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

  // @ts-ignore
  const id: number = pokemon?.id;
  // @ts-ignore
  const image: string = pokemon?.sprites?.other?.dream_world?.front_default;

  useEffect(() => {
    setLoading(true);
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setPokemon(data);
        setLoading(false);
      });
  }, [url]);

  return (
    <>
      {id <= 151 && !loading && (
        <PokemonTile>
          <Image alt={name} src={image} />

          <Link href={`/pokemon/${id}`} passHref>
            <Name>{name}</Name>
          </Link>
        </PokemonTile>
      )}
    </>
  );
};

export default PokemonItem;
