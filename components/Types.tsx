import React, { FC } from 'react';
import Type from './Type';
import styled from 'styled-components';

const StyledTypes = styled.ul`
  display: flex;
  justify-content: center;
`;

const StyledType = styled(Type)`
  &:not(:first-child) {
    margin-left: 0.833rem;
  }
`;

type TypesProps = {
  types: Array<{
    type: {
      name: string;
    };
  }>;
};

const Types: FC<TypesProps> = ({ types }) => (
  <StyledTypes>
    {types.map(({ type: { name } }, index) => {
      return <StyledType key={index} name={name} />;
    })}
  </StyledTypes>
);

export default Types;
