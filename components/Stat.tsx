import React, { FC } from 'react';
import styled from 'styled-components';

const StyledStat = styled.li`
  text-align: center;
  text-transform: capitalize;
`;

type StatProps = {
  base_stat: number;
  className?: string;
  stat: {
    name: string;
  };
};

const Stat: FC<StatProps> = ({ className, base_stat, stat: { name } }) => (
  <StyledStat className={className}>
    {base_stat} - {name}
  </StyledStat>
);

export default Stat;
