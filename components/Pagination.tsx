import React, { FC, useEffect, useState } from 'react';
import styled from 'styled-components';

const StyledPagination = styled.div`
  display: flex;
  flex-wrap: wrap;
  list-style: none;
  justify-content: center;
  margin-top: 1.111rem;
  padding: 0;

  .pagination {
    &__btn {
      &:not(:first-child) {
        margin-left: 0.556rem;
      }
    }
  }
`;

const Button = styled.button`
  appearance: none;
  align-items: center;
  background: transparent;
  border: 2px solid #fff;
  cursor: pointer;
  color: #fff;
  font-size: 1rem;
  padding: 0.556rem 1.111rem;
  text-align: center;
  text-decoration: none;

  &:not(:disabled):hover {
    background: #fff;
    color: #000;
  }

  :disabled {
    background-color: #d3d3d3;
    border: 1px solid #d3d3d3;
    color: #000;
    cursor: not-allowed;
  }

  &:not(:fist-child) {
    margin-left: 0.556rem;
  }

  &.pagination-btn {
    &--first,
    &--previous {
      .pagination-btn__text {
        margin-left: 0.556rem;
      }
    }

    &--next,
    &--last {
      .pagination-btn__text {
        margin-right: 0.556rem;
      }
    }
  }

  .pagination-btn {
    &__text {
      display: none;

      @media (min-width: 768px) {
        display: inline-block;
      }
    }

    &__icon {
      font-size: 1.333rem;
    }
  }
`;

type PaginationProps = {
  loading: boolean;
  offset: number;
  setOffset: (offset: number) => void;
};

const Pagination: FC<PaginationProps> = ({ loading, offset, setOffset }) => {
  const isDisabledNextOrLast = offset === 150 || loading;
  const isDisabledPrevOrFirst = offset === 0 || loading;

  const firstPage = () => setOffset(0);

  const prevPage = () => setOffset(offset - 25);

  const nextPage = () => setOffset(offset + 25);

  const lastPage = () => setOffset(150);

  return (
    <StyledPagination>
      <Button
        type="button"
        className="pagination__btn | pagination-btn pagination-btn--first"
        disabled={isDisabledPrevOrFirst}
        onClick={firstPage}
      >
        <span className="pagination-btn__icon">&#171;</span>
        <span className="pagination-btn__text">First</span>
      </Button>

      <Button
        type="button"
        className="pagination__btn | pagination-btn pagination-btn--previous"
        disabled={isDisabledPrevOrFirst}
        onClick={prevPage}
      >
        <span className="pagination-btn__icon">&#8249;</span>
        <span className="pagination-btn__text">Previous</span>
      </Button>

      <Button
        type="button"
        className="pagination__btn | pagination-btn pagination-btn--next"
        disabled={isDisabledNextOrLast}
        onClick={nextPage}
      >
        <span className="pagination-btn__text">Next</span>
        <span className="pagination-btn__icon">&#8250;</span>
      </Button>

      <Button
        type="button"
        className="pagination__btn | pagination-btn pagination-btn--last"
        disabled={isDisabledNextOrLast}
        onClick={lastPage}
      >
        <span className="pagination-btn__text">Last</span>
        <span className="pagination-btn__icon">&#187;</span>
      </Button>
    </StyledPagination>
  );
};

export default Pagination;
