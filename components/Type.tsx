import React, { FC } from 'react';
import styled from 'styled-components';

const StyledType = styled.li`
  background-color: #fff;
  border-radius: 5px;
  color: #000;
  padding: 0.278rem 0.556rem;
`;

type TypeProps = {
  className?: string;
  name: string;
};

const Type: FC<TypeProps> = ({ className, name }) => (
  <StyledType className={className}>{name}</StyledType>
);

export default Type;
