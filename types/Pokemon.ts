export type PokemonProps = {
  pokemon: {
    id: number;
    name: string;
    sprites: {
      other: {
        dream_world: {
          front_default: string;
        };
      };
    };
    types: Array<{
      type: {
        name: string;
      };
    }>;
    weight: number;
    height: number;
    stats: Array<{
      base_stat: number;
      stat: {
        name: string;
      };
    }>;
  };
};
