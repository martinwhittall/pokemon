export type AllPokemonProps = {
  pokemon: Array<{
    name: string;
    url: string;
  }>;
};
