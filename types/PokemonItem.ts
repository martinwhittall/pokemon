export type PokemonItemProps = {
  name: string;
  url: string;
};
