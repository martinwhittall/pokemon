import type { NextPage } from 'next';
import Head from 'next/head';
import { GetStaticProps } from 'next';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { AllPokemonProps } from './../types/AllPokemon';

import Pagination from '../components/Pagination';
import PokemonList from '../components/PokemonList';

const Header = styled.div`
  background-color: #d33a46;
  padding: 1.111rem;
`;

const Title = styled.h1`
  color: #fff;
`;

const Main = styled.div`
  padding: 1.111rem;
`;

const Home: NextPage<AllPokemonProps> = ({ pokemon }) => {
  const [allPokemon, setAllPokemon] = useState(pokemon);
  const [offset, setOffset] = React.useState(0);
  const [loading, setLoading] = React.useState(false);

  useEffect(() => {
    fetch(`https://pokeapi.co/api/v2/pokemon/?offset=${offset}&limit=20`)
      .then((response) => response.json())
      .then((data) => {
        setAllPokemon(data.results);
        setLoading(false);
      });
  }, [offset]);

  return (
    <div>
      <Head>
        <title>Home | Pokédex</title>
        <link rel="icon" href="./favicon.ico" />
      </Head>

      <Header>
        <Title>Pokédex</Title>
      </Header>

      <Main>
        <PokemonList allPokemon={allPokemon} />
        <Pagination loading={loading} offset={offset} setOffset={setOffset} />
      </Main>
    </div>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  const res = await fetch(
    `https://pokeapi.co/api/v2/pokemon/?offset=0&limit=20`,
  );
  const data = await res.json();

  return {
    props: { pokemon: data.results },
  };
};

export default Home;
