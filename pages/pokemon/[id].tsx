import React from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import { GetStaticPaths, GetStaticProps } from 'next';
import { ParsedUrlQuery } from 'querystring';
import styled from 'styled-components';

import { PokemonProps } from './../../types/Pokemon';
import { PokemonItemProps } from './../../types/PokemonItem';

import Header from './../../components/Header';
import Height from './../../components/Height';
import Name from './../../components/Name';
import Stats from './../../components/Stats';
import Types from './../../components/Types';
import Weight from './../../components/Weight';

const StyledDetails = styled.div`
  .pokemon-details {
    &__contents {
      padding: 1.111rem;
      margin: 0 auto;
      max-width: 380px;
    }

    &__psychical {
      display: flex;
      justify-content: center;

      > :not(:first-child) {
        margin-left: 1.111rem;
      }
    }

    &__types,
    &__psychical,
    &__stats {
      margin-top: 0.833rem;
    }
  }
`;

const PokemonDetails: NextPage<PokemonProps> = ({
  pokemon: { name, sprites, id, types, weight, height, stats },
}) => {
  const image: string = sprites?.other?.dream_world?.front_default;

  return (
    <>
      <Head>
        <title>{name} | Pokédex</title>
        <link rel="icon" href="./favicon.ico" />
      </Head>

      <StyledDetails className="pokemon-details">
        <Header id={id} name={name} image={image} />

        <div className="pokemon-details__contents">
          <Name name={name} />

          <div className="pokemon-details__types">
            <Types types={types} />
          </div>

          <div className="pokemon-details__psychical">
            <Weight weight={weight} />
            <Height height={height} />
          </div>

          <div className="pokemon-details__stats">
            <Stats stats={stats} />
          </div>
        </div>
      </StyledDetails>
    </>
  );
};

export const getStaticPaths: GetStaticPaths = async () => {
  const res = await fetch('https://pokeapi.co/api/v2/pokemon/?limit=151');
  const data = await res.json();
  const results = data.results;

  const paths = results.map((pokemon: PokemonItemProps | null) => {
    // @ts-ignore
    const id = pokemon.url.match(/\/pokemon\/(\d*)/)[1];

    return {
      params: { id: id.toString() },
    };
  });

  return {
    paths,
    fallback: false,
  };
};

interface Params extends ParsedUrlQuery {
  id: string;
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { id } = context.params as Params;
  const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${id}`);
  const data = await res.json();

  return {
    props: { pokemon: data },
  };
};

export default PokemonDetails;
