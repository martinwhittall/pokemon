## Getting Started

Using npm
Install the main project dependencies:

```bash
nvm use
npm install
```

Run the development server:

```bash
npm run-script build
npm run-script dev
```

Run the production server:

```bash
npm run-script build
npm run-script start
```

## TODO

- Refactor Pagination component
- Fix supressed TS issues
- Add test for index page
- Update favicon
- Add theme for variables

## Retro

- Improve accessibility
- Use scss modules over styled components
