import React from 'react';
import { render, screen } from '@testing-library/react';
import Details from './../../../pages/pokemon/[id]';

const props = {
  pokemon: {
    id: 1,
    name: 'bulbasaur',
    height: 7,
    sprites: {
      other: {
        dream_world: {
          front_default:
            'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg',
        },
      },
    },
    stats: [
      {
        base_stat: 45,
        stat: {
          name: 'hp',
        },
      },
      {
        base_stat: 49,
        stat: {
          name: 'attack',
        },
      },
      {
        base_stat: 49,
        stat: {
          name: 'defense',
        },
      },
      {
        base_stat: 65,
        stat: {
          name: 'special-attack',
        },
      },
      {
        base_stat: 65,
        stat: {
          name: 'special-defense',
        },
      },
      {
        base_stat: 45,
        stat: {
          name: 'speed',
        },
      },
    ],
    types: [
      {
        type: {
          name: 'grass',
        },
      },
      {
        type: {
          name: 'poison',
        },
      },
    ],
    weight: 69,
  },
};

describe('Details Page', () => {
  it('should render correctly', () => {
    const { asFragment } = render(<Details {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  it('should render back button', () => {
    render(<Details {...props} />);

    expect(screen.getAllByRole('button')).toHaveLength(1);
    expect(
      screen.getByRole('button', {
        name: /pokédex/i,
      }),
    );
  });

  it('should render pokemon ID', () => {
    render(<Details {...props} />);

    expect(screen.getByText(/#1/)).toBeInTheDocument();
  });

  it('should render pokemon image', () => {
    render(<Details {...props} />);

    const image = screen.getByRole('img');
    expect(image).toHaveAttribute(
      'src',
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg',
    );
    expect(image).toHaveAttribute('alt', 'bulbasaur');
  });

  it('should render pokemon name', () => {
    render(<Details {...props} />);

    expect(screen.getByText(/bulbasaur/i)).toBeInTheDocument();
  });

  it('should render pokemon weight', () => {
    render(<Details {...props} />);

    expect(screen.getByText(/69 kg/i)).toBeInTheDocument();
    expect(screen.getByText(/weight/i)).toBeInTheDocument();
  });

  it('should render pokemon height', () => {
    render(<Details {...props} />);

    expect(screen.getByText(/7 m/i)).toBeInTheDocument();
    expect(screen.getByText(/height/i)).toBeInTheDocument();
  });

  it('should render pokemon types', () => {
    render(<Details {...props} />);

    expect(screen.getByText(/grass/i)).toBeInTheDocument();
    expect(screen.getByText(/poison/i)).toBeInTheDocument();
  });

  it('should render pokemon base stats', () => {
    render(<Details {...props} />);

    expect(screen.getByText(/base stats/i)).toBeInTheDocument();
    expect(screen.getByText(/45 - hp/i)).toBeInTheDocument();
    expect(screen.getByText(/49 - attack/i)).toBeInTheDocument();
    expect(screen.getByText(/49 - defense/i)).toBeInTheDocument();
    expect(screen.getByText(/65 - special-attack/i)).toBeInTheDocument();
    expect(screen.getByText(/65 - special-defense/i)).toBeInTheDocument();
    expect(screen.getByText(/45 - speed/i)).toBeInTheDocument();
  });
});
