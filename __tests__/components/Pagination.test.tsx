import React from 'react';
import { render, screen } from '@testing-library/react';
import Pagination from './../../components/Pagination';

const mockedSetOffset = jest.fn();

const props = {
  loading: false,
  offset: 0,
  setOffset: mockedSetOffset,
};

describe('<Pagination />', () => {
  const setState = jest.fn();

  test('should render correctly', () => {
    const { asFragment } = render(<Pagination {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render pagination buttons', () => {
    render(<Pagination {...props} />);

    expect(screen.getAllByRole('button')).toHaveLength(4);
  });

  test('should render first and previous buttons disabled when on first page', () => {
    render(<Pagination {...props} />);

    expect(screen.getByText(/first/i).closest('button')).toBeDisabled();
    expect(screen.getByText(/previous/i).closest('button')).toBeDisabled();
    expect(screen.getByText(/next/i).closest('button')).toBeEnabled();
    expect(screen.getByText(/last/i).closest('button')).toBeEnabled();
  });

  test('should render all buttons enabled when on page 2', () => {
    const modifiedProps = { ...props, offset: 25 };

    render(<Pagination {...modifiedProps} />);

    expect(screen.getByText(/first/i).closest('button')).toBeEnabled();
    expect(screen.getByText(/previous/i).closest('button')).toBeEnabled();
    expect(screen.getByText(/next/i).closest('button')).toBeEnabled();
    expect(screen.getByText(/last/i).closest('button')).toBeEnabled();
  });

  test('should render next and last buttons disabled when on last page', () => {
    const modifiedProps = { ...props, offset: 150 };

    render(<Pagination {...modifiedProps} />);

    expect(screen.getByText(/first/i).closest('button')).toBeEnabled();
    expect(screen.getByText(/previous/i).closest('button')).toBeEnabled();
    expect(screen.getByText(/next/i).closest('button')).toBeDisabled();
    expect(screen.getByText(/last/i).closest('button')).toBeDisabled();
  });

  test('should render buttons disabled when loading pokemon', () => {
    const modifiedProps = { ...props, loading: true };

    render(<Pagination {...modifiedProps} />);

    expect(screen.getByText(/first/i).closest('button')).toBeDisabled();
    expect(screen.getByText(/previous/i).closest('button')).toBeDisabled();
    expect(screen.getByText(/next/i).closest('button')).toBeDisabled();
    expect(screen.getByText(/last/i).closest('button')).toBeDisabled();
  });
});
