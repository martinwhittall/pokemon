import React from 'react';
import { render, screen } from '@testing-library/react';
import Stat from '../../components/Stat';

const props = {
  base_stat: 45,
  stat: {
    name: 'hp',
  },
};

describe('<Stat />', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Stat {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render stat name', () => {
    render(<Stat {...props} />);

    expect(screen.getByText(/hp/)).toBeInTheDocument();
  });

  test('should render base_stat', () => {
    render(<Stat {...props} />);

    expect(screen.getByText(/45/)).toBeInTheDocument();
  });
});
