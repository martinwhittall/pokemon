import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from '../../components/Header';

const props = {
  id: 1,
  name: 'bulbasaur',
  image:
    'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg',
};

describe('<Header />', () => {
  test('should render correctly', () => {
    render(<Header {...props} />);

    const { asFragment } = render(<Header {...props} />);
    expect(asFragment()).toMatchSnapshot();
  });

  test('should render pokemon ID', () => {
    render(<Header {...props} />);

    expect(screen.getByText(/#1/)).toBeInTheDocument();
  });

  test('should render image with alt tag', () => {
    render(<Header {...props} />);

    const image = screen.getByRole('img');
    expect(image).toHaveAttribute(
      'src',
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg',
    );
    expect(image).toHaveAttribute('alt', 'bulbasaur');
  });

  test('should render back button', () => {
    render(<Header {...props} />);

    expect(
      screen.getByRole('button', {
        name: /pokédex/i,
      }),
    );
  });
});
