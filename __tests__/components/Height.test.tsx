import React from 'react';
import { render, screen } from '@testing-library/react';
import Height from './../../components/Height';

const props = {
  height: 7,
};

describe('<Height />', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Height {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render height', () => {
    render(<Height {...props} />);

    expect(screen.getByText(/7 m/)).toBeInTheDocument();
    expect(screen.getByText('Height')).toBeInTheDocument();
  });
});
