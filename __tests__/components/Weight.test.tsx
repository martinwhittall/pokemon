import React from 'react';
import { render, screen } from '@testing-library/react';
import Weight from './../../components/Weight';

const props = {
  weight: 69,
};

describe('<Weight />', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Weight {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render weight', () => {
    render(<Weight {...props} />);

    expect(screen.getByText(/69 kg/)).toBeInTheDocument();
    expect(screen.getByText('Weight')).toBeInTheDocument();
  });
});
