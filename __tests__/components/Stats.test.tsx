import React from 'react';
import { render, screen } from '@testing-library/react';
import Stats from '../../components/Stats';

const props = {
  stats: [
    {
      base_stat: 45,
      stat: {
        name: 'hp',
      },
    },
    {
      base_stat: 49,
      stat: {
        name: 'attack',
      },
    },
    {
      base_stat: 49,
      stat: {
        name: 'defense',
      },
    },
    {
      base_stat: 65,
      stat: {
        name: 'special-attack',
      },
    },
    {
      base_stat: 65,
      stat: {
        name: 'special-defense',
      },
    },
    {
      base_stat: 45,
      stat: {
        name: 'speed',
      },
    },
  ],
};

describe('<Stats />', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Stats {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render title', () => {
    render(<Stats {...props} />);

    expect(screen.getByText(/Base stats/)).toBeInTheDocument();
  });

  test('should render hp stat', () => {
    render(<Stats {...props} />);

    expect(screen.getByText(/45 - hp/)).toBeInTheDocument();
  });

  test('should render attack stat', () => {
    render(<Stats {...props} />);

    expect(screen.getByText(/49 - attack/)).toBeInTheDocument();
  });

  test('should render special-attack stat', () => {
    render(<Stats {...props} />);

    expect(screen.getByText(/65 - special-attack/)).toBeInTheDocument();
  });

  test('should render special-defense stat', () => {
    render(<Stats {...props} />);

    expect(screen.getByText(/65 - special-defense/)).toBeInTheDocument();
  });

  test('should render speed stat', () => {
    render(<Stats {...props} />);

    expect(screen.getByText(/45 - speed/)).toBeInTheDocument();
  });
});
