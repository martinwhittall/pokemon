import React from 'react';
import { render, screen } from '@testing-library/react';
import Name from '../../components/Name';

const props = {
  name: 'bulbasaur',
};

describe('<Name />', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Name {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render name', () => {
    render(<Name name={props.name} />);

    expect(screen.getByText(/bulbasaur/)).toBeInTheDocument();
  });
});
