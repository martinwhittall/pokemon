import React from 'react';
import { render, screen } from '@testing-library/react';
import Type from '../../components/Type';

const props = {
  name: 'grass',
};

describe('<Type />', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Type {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render type name', () => {
    render(<Type name={props.name} />);

    expect(screen.getByText(/grass/)).toBeInTheDocument();
  });
});
