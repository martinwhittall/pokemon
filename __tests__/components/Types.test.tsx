import React from 'react';
import { render, screen } from '@testing-library/react';
import Types from '../../components/Types';

const props = {
  types: [
    {
      type: {
        name: 'grass',
      },
    },
    {
      type: {
        name: 'poison',
      },
    },
  ],
};

describe('<Types />', () => {
  test('should render correctly', () => {
    const { asFragment } = render(<Types {...props} />);

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render types', () => {
    render(<Types {...props} />);

    expect(screen.getByText(/grass/)).toBeInTheDocument();
    expect(screen.getByText(/poison/)).toBeInTheDocument();
  });
});
