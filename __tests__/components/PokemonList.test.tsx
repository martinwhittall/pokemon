import React from 'react';
import { act, render, screen } from '@testing-library/react';
import PokemonList from '../../components/PokemonList';

const props = {
  allPokemon: [
    {
      name: 'bulbasaur',
      url: 'https://pokeapi.co/api/v2/pokemon/1',
    },
  ],
};

beforeEach(() => {
  // @ts-ignore
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () =>
        Promise.resolve({
          id: 1,
          name: 'bulbasaur',
          height: 7,
          sprites: {
            other: {
              dream_world: {
                front_default:
                  'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg',
              },
            },
          },
          stats: [
            {
              base_stat: 45,
              stat: {
                name: 'hp',
              },
            },
            {
              base_stat: 49,
              stat: {
                name: 'attack',
              },
            },
            {
              base_stat: 49,
              stat: {
                name: 'defense',
              },
            },
            {
              base_stat: 65,
              stat: {
                name: 'special-attack',
              },
            },
            {
              base_stat: 65,
              stat: {
                name: 'special-defense',
              },
            },
            {
              base_stat: 45,
              stat: {
                name: 'speed',
              },
            },
          ],
          types: [
            {
              type: {
                name: 'grass',
              },
            },
            {
              type: {
                name: 'poison',
              },
            },
          ],
          weight: 69,
        }),
    }),
  );
});

describe('<PokemonList />', () => {
  test('should render correctly', async () => {
    const { asFragment } = render(<PokemonList {...props} />);

    await act(() => Promise.resolve());

    expect(asFragment()).toMatchSnapshot();
  });

  test('should render list of pokemon', async () => {
    await act(async () => render(<PokemonList {...props} />));

    expect(screen.getAllByRole('link')).toHaveLength(1);
  });

  test('should render pokemon name', async () => {
    await act(async () => render(<PokemonList {...props} />));

    expect(screen.getByText(/bulbasaur/)).toBeInTheDocument();
  });

  test('should render pokemon image with alt tag', async () => {
    await act(async () => render(<PokemonList {...props} />));

    const image = screen.getByRole('img');
    expect(image).toHaveAttribute(
      'src',
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/1.svg',
    );
    expect(image).toHaveAttribute('alt', 'bulbasaur');
  });

  test('should render pokemon link', async () => {
    await act(async () => render(<PokemonList {...props} />));

    expect(screen.getByRole('link')).toHaveAttribute('href', '/pokemon/1');
  });
});
